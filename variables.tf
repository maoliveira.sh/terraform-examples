variable "region" {
    default = "us-east-2"
}

variable "instance_type" {
    default = "t3.micro"
}

variable "name" {
    default = "example" 
}

variable "lista" {
    default =   ["temp1", "temp2"]
}