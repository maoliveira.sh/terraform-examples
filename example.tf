provider "aws" {
    profile = "default"
    region  = var.region
}

resource "aws_instance" "example-1" {
    count           = 1
    ami             = "ami-0a91cd140a1fc148a"
    instance_type   = var.instance_type
    
    tags            = {
        Name = "${var.name}-${count.index}"
        lista = var.lista[0]
    }
}


