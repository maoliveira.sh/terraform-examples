provider "aws" {
    profile = "default"
    region  = "us-east-2"
}


resource "aws_instance" "count" {
    count           = 1
    ami             = "ami-0a91cd140a1fc148a"
    instance_type   = "t3.micro"
    
    tags            = {
        Name = "count"
    }
}